//print spell of given input
//input x=3
//
//
//
import java.io.*;
class SwitchDemo{
	public static void main(String[]  ss)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the value of x");
	int x=Integer.parseInt(br.readLine());

		switch(x){
			case 1: 
				System.out.println("ONE");
				break;
			case 2:
				System.out.println("TWO");
				break;
			case 3:
				System.out.println("THREE");
				break;
			case 4:
				System.out.println("FOUR");
				break;
			case 5:
				System.out.println("FIVE");
				break;
			default:
				System.out.println("NO MATCH");
				break;
		}
			System.out.println("AFTER SWITCH");
	}
}	

