class StringDemo{
	public static void main(String[]args){
	
		String str1 = "KANHA";			//SCP
		String str2 = "KANHA";			//SCP
		String str3 = new String ("KANHA");	//HEAP
		String str4 = new String ("KANHA");	//HEAP
		String str5 = new String ("RAHUL");     //HEAP
		String str6 = "RAHUL";			//SCP

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(System.identityHashCode(str5));
		System.out.println(System.identityHashCode(str6));
	}
}
