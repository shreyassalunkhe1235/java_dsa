class StringDemo{
	public static void main(String[]args){
	
		String str1 = "SHASHI";		//SCP
		String str2 = "BAGAL";		//SCP

		System.out.println(str1 + str2);
		String str5 = "BHUSHAN";
		String str3 = "SHASHIBAGAL";	//SCP
		String str4 = str1 + str2;	//HEAP

		System.out.println(System.identityHashCode(str1+str2));
		System.out.println(System.identityHashCode(str5+str2));
		System.out.println(System.identityHashCode("SHASHI"));
		System.out.println(System.identityHashCode(str1)); 
	}
}
