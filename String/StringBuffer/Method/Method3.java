//Method3 : public synchronized stringbuffer delete(int start ,int end)
//Parameter: integer(start index from which to delete string)
//	     integer(end  index upto which string want to delete )
//Return type = sytringBuffer	     		


class DeleteDemo{
	public static void main(String [] args){
	
		StringBuffer sb = new StringBuffer("Core2Web");
		sb.delete(2,7);
		System.out.println(sb);

	}
}
