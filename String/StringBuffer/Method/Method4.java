// Method 4 : public synchronized stringBuffer reverse()
// Parameter : no parameter 
// Return type : StringBuffer


class ReverseDemo{
	public static void main(String[]args){
	
		StringBuffer sb = new StringBuffer("Shashi");
		System.out.println(sb.reverse());
	}
}
