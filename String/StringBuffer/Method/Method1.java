//Method 1 : public synchronized stringbuffer append(string)
//		str1.append(str2);
//Parameter : String 
//Return Value : StringBuffer
//


class AppendDemo{
	public static void main(String[]args){
	
		StringBuffer sb1 = new StringBuffer("ShashiBaagal");
		StringBuffer sb2 = new StringBuffer("Sir");
		StringBuffer sb3 = sb1.append(sb2);
		System.out.println(sb3);
	}
}
