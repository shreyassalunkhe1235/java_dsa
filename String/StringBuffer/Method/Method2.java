//Method 2 : public synchronized stringbuffer insert(int ofset, string str)
//Parameter : insert (index from where the string will insert)
//Return type : StringBuffer


class InsertDemo{
	public static void main(String[]args){
	
		StringBuffer sb1 = new StringBuffer("ShashiBagal");
		System.out.println(sb1.capacity());

		System.out.println(sb1);
		System.out.println(System.identityHashCode(sb1));

		sb1.insert(6,"Sir");
		System.out.println(sb1.capacity());

		sb1.insert(2,"Core2Web");
		System.out.println(sb1.capacity());
		
		
		System.out.println(sb1);
		System.out.println(System.identityHashCode(sb1));
	}
}
