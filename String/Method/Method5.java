//METHOD : public int compareToIgnoreCase(String str)
//description : it compares str1 and str2 (case sensitive) 
//return type : integer
//


class CompareToIgnoreCaseDemo{
	public static void main(String [] args){
	
		String str1 = "SHREYAS";
		String str2 = "SHREYAS";


		System.out.println(str1.compareToIgnoreCase(str2));
	
	}
}
