//Method 10 : public int lastIndexOf(char ch , int from index)
//Description : find the last instance of the character in the given string
//Parameter : character (ch upto find) , integer (index to starts the search)
//return type : integer



class LastIndexOdDemo{
	public static void main(String[]args){
	
		String str1 = "ShashiBaagal";

		System.out.println(str1.lastIndexOf('h', 2));   //1
		System.out.println(str1.lastIndexOf('a',0));    //-1
	}	

}

