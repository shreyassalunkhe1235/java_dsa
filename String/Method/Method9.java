//Method 9 : public int indexOf(char ch , int from index)
//Description : find first instance of character
//		in the given string
//Parameter : character (ch to find)integer (index to start the searh)
//retuen type : integer


class IndexOfDemo{
	public static void main(String[]args){
	
		String str1 = "Shashi";
		
		System.out.println(str1.indexOf('h',0));
		System.out.println(str1.indexOf('a',2));
	}
}


