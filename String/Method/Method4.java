//METHOD 4 : public int compareTo(String str2);
//Description : it compares the str1 and str2 (case sensitive),
//		if both the string are equal it returns 0 otherqwise
//		returns the comparision
//parameter : string(second string)
//return type : integer



class CompareToDemo{
	public static void main(String [] args){
	
		String str1 = "Ashish";
		String str2 = "ashish";
	System.out.println(str1.compareTo(str2));

	}
}



/*HERE STR1 IS COMPARE WITH STR2 .
 THIS IS CASE SENSITIVE METHOD 
 SO IT GIVE DIFFERNCE BETWEEN CHAR (ACQII VALUE )
  	A - a =-32
*/	
