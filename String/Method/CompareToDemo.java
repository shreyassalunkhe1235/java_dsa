import java.util.*;
class CompareToDemo{
	static int myCompareTo(String str1,String str2){
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		if(arr1.length==arr2.length){
		   for(int i=0; i<arr1.length; i++){
		   	for(int j=i; j<arr2.length; j++){
			
			    if(arr1[i]==arr2[j]){
				break;
			    }
			    else{
			    	return arr1[i]-arr2[j];
			    }
			}
		   }
		return 0;
		}
		else{
		    return str1.length()-str2.length();
		}
	}	
		
	public static void main(String [] args){
	     Scanner sc = new Scanner(System.in);

	     System.out.println("Enter two string : ");
	      String str1 = sc.next();
	      String str2 = sc.next();

	      System.out.print(myCompareTo(str1,str2));
	}
}
