//Method 12 : public Stringsubstring(int index)
//Description : creates substring of the given string starting a
//		at a specifidd index and ending at the end of the string
//Parameter : integer (index of the string)
//Return type : string 
//

class SubstringDemo{
	public static void main(String[]args){
	
		String str1 = "ShashiBagalSir";

		System.out.println(str1.substring(5));
		System.out.println(str1.substring(2,6));
	}
}
