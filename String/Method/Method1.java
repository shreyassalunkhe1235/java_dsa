//METHOD1 : public String concat(String str)
//Description : Concatinate string to the string i.e., another string is concanitinated with
//              the first first string
//              Implements new array of character whose length is sum of str1.length  and
//              str2.length 
//parameter : string
//return type : String 
//




class ConcateDemo{
	public static void main(String[]args){
	
		String str1 = "Core2";
		String str2 = "Web";

		String str3 = str1.concat(str2);
		System.out.println(str3);
	}
}
