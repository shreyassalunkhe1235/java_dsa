class StringDemo{
	public static void main(String[]args){
	
		char ch = 'A';
		char ch1 = 'A';		//INTEGER CACHE
		int x = 65;		//INTEGER CACHE

		System.out.println(System.identityHashCode(ch));
		System.out.println(System.identityHashCode(ch1));
		System.out.println(System.identityHashCode(x));

	}
}
