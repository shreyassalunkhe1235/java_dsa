class StringDemo{
	public static void main(String[]arg){
	
		String str1 = "Core2Web";		//STRING CONSTANT POOL
		String str2 = new String("Core2Web");	//HEAP

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		String str3 = "Core2Web";		//STRING CONSTANT POOL
		String str4 = new String("Core2Web");	//HEAP

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));

	}
}
