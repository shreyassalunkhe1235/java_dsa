/*Take number of rows from user
output :3 b 1 d
	a 2 c 0
	3 b 1 d
	a 2 c 0
*/

import java.util.*;
class Pattern4{

        public static void main(String ar[]){

                Scanner sc = new Scanner(System.in);

                int row = sc.nextInt();
                        
                for(int i = 1; i <= row; i++){
			int n = 3;
			int ch = 'a';

                        for(int j = 1; j <= row; j++){

                               if(i%2==1){
			       	
				       if(j%2==1){
				       	System.out.print(n+" ");

				       }else{
				       	System.out.print((char)ch+" ");
				       }
				       n--;
				       ch++;

			       }else{
			       	       if(j%2==1){
                                        System.out.print((char)ch+" ");

                                       }else{
                                        System.out.print(n+" ");
                                       }
                                       n--;
                                       ch++;
			       }
                        }

                        System.out.println();
                }
        }
}

