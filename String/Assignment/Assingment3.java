/*
QUE3.take number of rows from user
0  1  1  2
3  5  8 13
21 34 55 89
*/

import java.util.*;
class Pattern3{

        public static void main(String[] arg){

                Scanner sc = new Scanner(System.in);
		System.out.println("Enter the No of rows ");
                int Row = sc.nextInt();
		int n = 0;
		int k = 1;
                        
                for(int i = 1; i<=Row; i++){

                        for(int j = 1; j<= Row+1; j++){

                                System.out.print(n+" ");
				int temp = n + k;	
				n=k;
				k=temp;
                                
                        }

                        System.out.println();
                }
        }
}

