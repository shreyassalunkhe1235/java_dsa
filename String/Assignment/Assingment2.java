
//take number of rows from user

//output :D C B A
//	  e d c b
//	  F E D C
//        g f e d
//


import java.util.*;
class Pattern2{
	public static void main(String[] arg){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the no of rows"  );
		int Rows = sc.nextInt();
		int n = 68;

		for(int i=1; i<=Rows;i++){
			int temp=n;
			for(int j=1;j<=Rows;j++){
			
				if(i%2!=0){
				
					System.out.print((char)n +" ");
					n--;
				}else{
					int k=n+32;
					System.out.print((char)k+" ");
					n--;
				}
			}
			n=temp;
			n++;
			System.out.println();
		}

	}
}	
