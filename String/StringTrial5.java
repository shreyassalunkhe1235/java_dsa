class StringDemo{
	public static void main(String[]args){
		
		String str1 = "KANHA";		//SCP
		String str2 = str1;		//SCP
		String str3 = new String(str2);	//HEAP

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));

	}
}
