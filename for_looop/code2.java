//Print odd number from 1 to n .Take n from user.
//Input : 6
//Output : 1 3 5
//
import java.util.*;
class Odd{
	public static void main(String[]args){
	Scanner sc=new Scanner(System.in);
	int i=sc.nextInt();
        
 	for(int n=1; n<=i; n++){
	if(n%2!=0){
	System.out.println(n);
	}
	}	
	}
}
