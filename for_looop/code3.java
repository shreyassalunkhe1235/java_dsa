//Print factorial of given number.
//Input : 6
//output : 720
//

import java.util.*;
class Factorial{
	public static void main(String[]args){
	Scanner sc=new Scanner(System.in);
	int i=sc.nextInt();
	int fact=1;
	for(int n=1; n<=i; n++){
	fact=fact*n;	
	System.out.println(fact);
	}
	}
}
