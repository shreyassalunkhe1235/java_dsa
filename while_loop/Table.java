//Write a program to print table of 2
//input :- 2
//output :- 2
//	    4
//	    6
//	    8
//	    10
//
//	    .
//	    .
//	    .
//	    .
//	    .
//	    20

import java.util.*;
class Table{
	public static void main(String[]args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the number : ");
	int i=sc.nextInt();
	while(i<=20){
	if(i%2==0)
		System.out.println(i);
	i++;
	}
	}
}
