//que3.take an integer n as input
//print multiplication of its digit
//
//input:6531
//output:90
import java.util.*;
class Multi{
	public static void main(String[]args){
	 Scanner sc=new Scanner(System.in);
	 int i=sc.nextInt();
	 int multi=1;
	 while(i!=0){              //0(false)i
		 int n=i%10;    //3 ,2 ,1 
		 multi=multi*n;   //1*3=3 ,3*2=6 ,6*1=6

	 	 i=i/10;       //123/10=12 ,12/10=1  ,1/10=0 
	 }  
		System.out.println(multi);
	}

}
