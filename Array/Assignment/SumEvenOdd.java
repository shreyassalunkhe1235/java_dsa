//WRITE A JAVA [ROGRAM TO FIND THE SUM OF EVEN AND ODD NUMBERS IN AN ARRAY
//DISPLAY THE SUM VALUE
//INOUT: 11 12 13 14 15 
//OUTPUT : SUM OF EVEN: 
//	   SUM OF ODD:
//



import java.io.*;
class SumEvenOdd{
	public static void main(String[] ss)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array : ");
		int size=Integer.parseInt(br.readLine());
		//int arr[]=new arr[size];

		System.out.println("Enter the numbers : ");
		int sumeven=0;
		int sumodd=0;
		for(int i=0 ; i<size ; i++ ){
		    int n=Integer.parseInt(br.readLine());
		    if(n%2==0){
		       sumeven=sumeven+n;	
		    }
		    else{
		      sumodd=sumodd+n;
		    }
		} 
	      System.out.println("Sum of even : "+ sumeven);
      	      System.out.println("Sum of odd : "+ sumodd);

	}
}
