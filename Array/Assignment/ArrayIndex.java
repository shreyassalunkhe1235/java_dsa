//WAp to take size of array from user and also take integer elements from user print product of odd index only 
 
 //INPUT: Enter size :6
  //enter array elements :1 2 3 4 5 6
 //OUTPUT:48
 //
 

import java.util.*;
class ArrayIndex{
	public static void main(String[] ss){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size : ");

		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter array elements : ");
		int mult=1;
		
		for(int i=0; i<size; i++){
			int arr1=sc.nextInt();
		
			if(i%2==1){
			 mult=mult*arr1;
			}
		}
		System.out.println("Product of odd index = "+ mult);
	}
}
