//RITE A PROGRSM TO FIND THE NUMBER OF EVEN AND ODD INTEGERS IN A GIVEN ARRAY OF INTEGERS
//INPUT : 1 2 3 4 5 6 7 8 9
//OUTPUT:
//
//




import java.io.*;
class EvenOdd{
        public static void main(String[]args)throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter size of the array : ");
        int size=Integer.parseInt(br.readLine());
	int n[]=new int[size];
        int even=0;
	int odd=0;

        System.out.println("Enter the elements in array : ");

          for(int i=0; i<size; i++){
                 int arr=Integer.parseInt(br.readLine());
		if(arr%2==0){
		 even++;
		} 
		else{
		 odd++;
		}
	  }	
	  System.out.println("Count of Even elements : " + even);
	  System.out.println("Count of odd elements : " + odd);
	}
}
