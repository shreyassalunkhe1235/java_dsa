//given an integer n
//print sum of its digit
//assume: n >= 0
//
//input: 6531
//output: 15 
import java.util.*;
class Addition{
	public static void main(String[]args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the digit :- ");
	int i=sc.nextInt();
	int sum=0;

	while(i!=0){
	  int n=i%10;
	  sum=sum+n;
	  i=i/10;
	 }
  	    System.out.println(sum);
	
	}
	
}



